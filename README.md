# sqlcmd
## Python implementation of sqlcmd. A commandline interface for Microsoft SQL Server.
### Installation

#### Unix *(Linux/Mac)*:
1. Clone the repo and copy all the files to your desired installation folder. *Default: `/opt/sqlcmd/`*
2. Install python dependencies listed in `requirements.txt`.
3. Link to program from a recognized software path. *Example Command: *
``` bash
sudo ln -s /opt/sqlcmd/sqlcmd /usr/bin/sqlcmd
```
4. Ensure files are set executable. *Example Commands: *
``` bash
sudo chmod +x /opt/sqlcmd/sqlcmd
sudo chmod +x /usr/bin/sqlcmd
```
5. Run `sqlcmd -h` to insure proper installation and view usage information.

#### Windows
*Unknown: Should run like any other python script assuming the requirements are 
installed as listed in requirements.txt*

### Version 3.1
### Authors/Contributors

#### Author:
###### Jane Keller: `<0@01010011101.com>`